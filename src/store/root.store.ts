import { create } from 'zustand'
import { type IViewSlice, createViewSlice } from './slices'

export type RootStore = IViewSlice

export const useRootStore = create<RootStore>()((...a) => ({
    ...createViewSlice(...a),
}))