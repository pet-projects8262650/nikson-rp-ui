import { APP_VIEWS } from 'common/constants/app.constants';
import { StateCreator } from 'zustand';

export interface IViewSlice {
  current_view: keyof typeof APP_VIEWS;
}

export const createViewSlice: StateCreator<
  IViewSlice,
  [],
  [],
  IViewSlice
> = () => ({
  current_view: 'PERSONA_CHOSE'
});
